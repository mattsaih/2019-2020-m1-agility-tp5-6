// Test d'exemple par défaut :
describe('The Traffic web site home page', () => {
    it('successfully loads', () => {
        cy.visit('/')
    })
})

describe('Scenario 1', () => {
    it('click on configuration', () => {
        cy.get('a[href ="#configuration"]').click()
    })
    it('verify that you are on configuration page', () => {
        cy.url().should('include', '/#configuration')
    })
})

describe('Scenario 2', () => {
    it('go to configuration page', () => {
        cy.get('a[href ="#configuration"]').click()
    })
    it('verify that there is 28 segments', () => {
        cy.get(':nth-child(1) > :nth-child(2) > .table > tbody ').find('tr').should('have.length', 28) //Ici je compte le nombre de lignes dans le tableau qui liste
    })                                                                                                 //tous les segments
})

describe('Scenario 3', () => {
    it('go to simulation page', () => {
        cy.get('a[href ="#simulation"]').click()
    })
    it('verify that there is no vehicles', () => {
        cy.get('.col-md-7 > .table').contains('tr', 'No vehicle available') //Je vérifie que le tableau qui répertorie les véhicules comporte bien la case stipulant
    })                                                                      //qu'il n'y a pas de véhicules
})

describe('Scenario 4', () => {
    it('go to configuration page', () => {
        cy.get('a[href ="#configuration"]').click()
    })
    it('edit segment 5 speed to 30', () => {
        cy.get('input[name = "speed"][form = "segment-5"]').clear().type('30')
        cy.get('#segment-5 > .btn').click()
        cy.get('.modal-footer > .btn').click()
    })
    it('verify edit in API', () => {
        cy.request('GET', 'http://127.0.0.1:4567/elements').should((response) => {
            expect(response.body['segments'][4]).to.have.property('speed', 30)
        })
    })
})

describe('Scenario 5', () => {
    it('go to configuration page', () => {
        cy.get('a[href ="#configuration"]').click()
    })
    it('edit roundabout capacity and time', () => {
        cy.get('input[name = "capacity"][form = "roundabout-31"]').clear().type('4')
        cy.get('input[name = "duration"][form = "roundabout-31"]').clear().type('15')
        cy.get(':nth-child(3) > .card-body > form > .btn').click()
        cy.get('.modal-footer > .btn').click()
    })
    it('verify edit in API', () => {
        cy.request('GET', 'http://127.0.0.1:4567/elements').should((response) => {
            expect(response.body['crossroads'][2]).to.have.property('capacity', 4)
            expect(response.body['crossroads'][2]).to.have.property('duration', 15)
        })
    })
})

describe('Scenario 6', () => {
    it('go to configuration page', () => {
        cy.get('a[href ="#configuration"]').click()
    })
    it('edit traffic light 29', () => {
        cy.get('input[name = "orangeDuration"][form = "trafficlight-29"]').clear().type('4')
        cy.get('input[name = "greenDuration"][form = "trafficlight-29"]').clear().type('40')
        cy.get('input[name = "nextPassageDuration"][form = "trafficlight-29"]').clear().type('8')
        cy.get(':nth-child(1) > .card-body > form > .btn').click()
        cy.get('.modal-footer > .btn').click()
    })
    it('verify edit in API', () => {
        cy.request('GET', 'http://127.0.0.1:4567/elements').should((response) => {
            expect(response.body['crossroads'][0]).to.have.property('orangeDuration', 4)
            expect(response.body['crossroads'][0]).to.have.property('greenDuration', 40)
            expect(response.body['crossroads'][0]).to.have.property('nextPassageDuration', 8)
        })
    })
})

describe('Scenario 7', () => {
    it('go to configuration page', () => {
        cy.get('a[href ="#configuration"]').click()
    })
    it('add the first vehicle', () => {
        cy.get('input[name = "origin"][form = "addVehicle"]').clear().type('5')
        cy.get('input[name = "destination"][form = "addVehicle"]').clear().type('26')
        cy.get('input[name = "time"][form = "addVehicle"]').clear().type('50')
        cy.get('.col-md-4 > form > .btn').click()
        cy.get('.modal-footer > .btn').click()
    })
    it('add the second vehicle', () => {
        cy.get('input[name = "origin"][form = "addVehicle"]').clear().type('19')
        cy.get('input[name = "destination"][form = "addVehicle"]').clear().type('8')
        cy.get('input[name = "time"][form = "addVehicle"]').clear().type('200')
        cy.get('.col-md-4 > form > .btn').click()
        cy.get('.modal-footer > .btn').click()
    })
    it('add the third vehicle', () => {
        cy.get('input[name = "origin"][form = "addVehicle"]').clear().type('27')
        cy.get('input[name = "destination"][form = "addVehicle"]').clear().type('2')
        cy.get('input[name = "time"][form = "addVehicle"]').clear().type('150')
        cy.get('.col-md-4 > form > .btn').click()
        cy.get('.modal-footer > .btn').click()
    })
    it('verify edit in API', () => {
        cy.request('GET', 'http://127.0.0.1:4567/vehicles').should((response) => {
            expect(response.body['50.0'][0]).exist
            expect(response.body['200.0'][0]).exist
            expect(response.body['150.0'][0]).exist
        })
    })
    it('verify edit in GUI', () => {
        cy.get('.col-md-8 > .table > tbody').find('tr').should('have.length', 3) //Je vérifie que le tableau qui répertorie les véhicules comporte bien 3 lignes donc
    })                                                                           //3 véhicules
})

describe('Scenario 8', () => {
    it('go to simulation page', () => {
        cy.get('a[href ="#simulation"]').click()
    })
    it('verify that all vehicles are stopped', () => {
        cy.get(':nth-child(1) > :nth-child(6) > div').contains('block') //Je vérifie que les véhicules ont bien l'icône indiquant qu'ils sont arrêtés
        cy.get(':nth-child(2) > :nth-child(6) > div').contains('block')
        cy.get(':nth-child(3) > :nth-child(6) > div').contains('block')
    })
    it('set and lauch simulation', () => {
        cy.get('input[name = "time"][form = "runNetwork"]').clear().type('120')
        cy.get('.btn').click()
    })
    it('wait the end of the simulation and verify its finished', () => {
        cy.get('.progress-bar', { timeout: 35000 }).should('have.attr', 'aria-valuenow', '100')
    })
    it('verify that only the fist vehicle is moving', () => {
        cy.get(':nth-child(1) > :nth-child(6) > div').contains('block')
        cy.get(':nth-child(2) > :nth-child(6) > div').contains('block')
        cy.get(':nth-child(3) > :nth-child(6) > div').contains('play_circle_filled') //Je vérifie que seul ce véhicule a bien l'icône indiquant qu'il est en mouvement
    })
})

describe('Scenario 9', () => {
    it('reload the page', () => {
        cy.request ('GET','http://127.0.0.1:3000/init')
        cy.reload()
    })
    it('verify that vehicles have been deleted', () => {
        cy.get('a[href ="#simulation"]').click()
        cy.get('.col-md-7 > .table').contains('tr', 'No vehicle available') //Je vérifie que le tableau qui répertorie les véhicules comporte bien la case stipulant
    })                                                                      //qu'il n'y a pas de véhicules
    it('add the vehicles', () => {
        cy.get('a[href ="#configuration"]').click()

        //First vehicle
        cy.get('input[name = "origin"][form = "addVehicle"]').clear().type('5')
        cy.get('input[name = "destination"][form = "addVehicle"]').clear().type('26')
        cy.get('input[name = "time"][form = "addVehicle"]').clear().type('50')
        cy.get('.col-md-4 > form > .btn').click()
        cy.get('.modal-footer > .btn').click()

        //Second vehicle
        cy.get('input[name = "origin"][form = "addVehicle"]').clear().type('19')
        cy.get('input[name = "destination"][form = "addVehicle"]').clear().type('8')
        cy.get('input[name = "time"][form = "addVehicle"]').clear().type('200')
        cy.get('.col-md-4 > form > .btn').click()
        cy.get('.modal-footer > .btn').click()

        //Third vehicle
        cy.get('input[name = "origin"][form = "addVehicle"]').clear().type('27')
        cy.get('input[name = "destination"][form = "addVehicle"]').clear().type('2')
        cy.get('input[name = "time"][form = "addVehicle"]').clear().type('150')
        cy.get('.col-md-4 > form > .btn').click()
        cy.get('.modal-footer > .btn').click()
    })
    it('set, lauch and waiting of the end of the simulation', () => {
        cy.get('a[href ="#simulation"]').click()
        cy.get('input[name = "time"][form = "runNetwork"]').clear().type('500')
        cy.get('.btn').click()
        cy.get('.progress-bar', { timeout: 500000 }).should('have.attr', 'aria-valuenow', '100')
    })
    it('verify that vehicles are not moving', () => {
        cy.get(':nth-child(1) > :nth-child(6) > div').contains('block') //Je vérifie que les véhicules ont bien l'icône indiquant qu'ils sont arrêtés
        cy.get(':nth-child(2) > :nth-child(6) > div').contains('block')
        cy.get(':nth-child(3) > :nth-child(6) > div').contains('block')
    })
})

describe('Scenario 10', () => {
    it('reload the page', () => {
        cy.request ('GET','http://127.0.0.1:3000/init')
        cy.reload()
    })
    it('verify that vehicles have been deleted', () => {
        cy.get('a[href ="#simulation"]').click()
        cy.get('.col-md-7 > .table').contains('tr', 'No vehicle available') //Je vérifie que le tableau qui répertorie les véhicules comporte bien la case stipulant
    })                                                                      //qu'il n'y a pas de véhicules
    it('add the vehicles', () => {
        cy.get('a[href ="#configuration"]').click()

        //First vehicle
        cy.get('input[name = "origin"][form = "addVehicle"]').clear().type('5')
        cy.get('input[name = "destination"][form = "addVehicle"]').clear().type('26')
        cy.get('input[name = "time"][form = "addVehicle"]').clear().type('50')
        cy.get('.col-md-4 > form > .btn').click()
        cy.get('.modal-footer > .btn').click()

        //Second vehicle
        cy.get('input[name = "origin"][form = "addVehicle"]').clear().type('5')
        cy.get('input[name = "destination"][form = "addVehicle"]').clear().type('26')
        cy.get('input[name = "time"][form = "addVehicle"]').clear().type('80')
        cy.get('.col-md-4 > form > .btn').click()
        cy.get('.modal-footer > .btn').click()

        //Third vehicle
        cy.get('input[name = "origin"][form = "addVehicle"]').clear().type('5')
        cy.get('input[name = "destination"][form = "addVehicle"]').clear().type('26')
        cy.get('input[name = "time"][form = "addVehicle"]').clear().type('80')
        cy.get('.col-md-4 > form > .btn').click()
        cy.get('.modal-footer > .btn').click()
    })
    it('set, lauch and waiting of the end of the simulation', () => {
        cy.get('a[href ="#simulation"]').click()
        cy.get('input[name = "time"][form = "runNetwork"]').clear().type('200')
        cy.get('.btn').click()
        cy.get('.progress-bar', { timeout: 5000000 }).should('have.attr', 'aria-valuenow', '100')
    })
    it('verify that vehicles are on the right segments', () => {
        cy.get('tbody > :nth-child(1) > :nth-child(5)').should('have.text','29') //Je vérifie sur quel segment se trouve chaque véhicule dans le tableau 
        cy.get('tbody > :nth-child(2) > :nth-child(5)').should('have.text','29')
        cy.get('tbody > :nth-child(3) > :nth-child(5)').should('have.text','17')
    })
})
